import sys
from frontend import front_end as application


if __name__ == '__main__':
    app = application.app
    main = application.Main()
    main.show()
    sys.exit(app.exec_())