
from os import listdir, mkdir
from os.path import isfile, join, exists, exists
from wand.image import Image
import argparse

def add_watermark_to_image(watermark_file, input_dir, output_dir):
    """
    Simple script that sets watermark and resizes image to the given percentage.

    """
    # ===============================
    # Check if directory exists
    # ===============================
    def check_in_path(path):
        if not exists(path):
            raise "{0} directory does not exists.".format(path)
        return path


    # ===============================
    # Create output directory if does not exist
    # ===============================
    def create_out_path(out):
        if not exists(out):
            print("Creating directory {0}.".format(out))
            mkdir(out)
            print("Created directory {0}.".format(out))
        return out


    # ===============================
    # Check resize value. Values below 5 are not permitted
    # ===============================
    def check_resize_value(value):
        if (value < 5):
            raise NameError("Invalid resize value: " + value)
        return str(value) + '%'


    # ===============================
    # Determine orientation from exif
    # ===============================
    def orientation(img):
        for k, v in img.metadata.items():
            if k.startswith("exif:Orientation"):
                return v


    # ===============================
    # Rotate if necessary and set watermark
    # ===============================
    def watermark_position(img, watermark):
        orient = orientation(img)
        print("orientation = {0}".format(orient))
        if (orient == "8"):
            img.rotate(-90)
            set_watermark(img, watermark)
            img.rotate(90)
        elif (orient == "6"):
            img.rotate(90)
            set_watermark(img, watermark)
            img.rotate(-90)
        else:
            set_watermark(img, watermark)


    # ===============================
    # Set watermark at the bottom-left corner of the image
    # ===============================
    def set_watermark(img, watermark):
        img.watermark(watermark, transparency=0.60, left=15, top=img.height - watermark.height - 15)


    """
    BEGIN
    """
    # # Parse args
    # parser = argparse.ArgumentParser(description='Set watermark on left-bottom corner and resize image.')
    # parser.add_argument('-w', "--watermark", action="store", dest="w",
    #                     help="watermark image to set. If missing, watermark will not be set.")
    # parser.add_argument('-i', "--input", action="store", dest="i", required=True, help="input directory (mandatory).")
    # parser.add_argument('-o', "--output", action="store", dest="o", default="/tmp/resize/", help="output directory.")
    # parser.add_argument('-r', "--resize", action="store", dest="r", default="100",
    #                     help="image resize percent. If missing will not resize image.")

    # args = parser.parse_args()

    # Checks
    in_path = check_in_path(input_dir)
    out_path = create_out_path(output_dir)
    
    if watermark_file:
        watermark = Image(filename=watermark_file)

    # Run
    #print("Resizing all files from {0}. Output to {1}".format(in_path, out_path))

    #know count of files


    files = []
    for f in listdir(in_path):
        filename = join(in_path, f)
        if isfile(filename):
            files.append(filename)

    global len_of_files
    len_of_files = len(files)
    global progress_iter
    progress_iter = 0
    


    for f in listdir(in_path):
        filename = join(in_path, f)
        #print(files)

        print("VIPOLNENO {} iz {}".format(progress_iter, len(files)))
        if isfile(filename):
            progress_iter += 1
            


            
            #print("---\n" + filename)
            #print("FILENAME", filename)
            try:
                with Image(filename=filename) as img:
                    print("Size before {0}.".format(img.size))
                    
                    if watermark_file:
                        watermark_position(img, watermark)
                    img.save(filename=output_dir + "/" + f)
                    #print("Size after {0}.".format(img.size)
                    
                    

            except Exception as e:
                print("Image processing error:", e)
                return 1

    print("Finished processing images.")
    return 0
    """
    END
    """