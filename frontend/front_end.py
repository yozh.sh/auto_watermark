import sys
from PyQt4 import QtGui, QtCore
from backend.add_watermark import add_watermark_to_image as add_wtm
from multiprocessing import Process, Lock

#backend

from os import listdir, mkdir
from os.path import isfile, join, exists, exists
from wand.image import Image
import argparse



app = QtGui.QApplication(sys.argv)

class MakeWatermark(QtCore.QThread):
    def __init__(self, watermark_file, input_dir, output_dir, parent=None):
        # QtCore.QThread.__init__(self, watermark_file, input_dir, output_dir, parent)
        super(QtCore.QThread, self).__init__()

        self.watermark_file = watermark_file
        self.input_dir = input_dir
        self.output_dir = output_dir 

    def run(self):
        def add_watermark_to_image(watermark_file, input_dir, output_dir):

            def check_in_path(path):
                if not exists(path):
                    raise "{0} directory does not exists.".format(path)
                return path

            def create_out_path(out):
                if not exists(out):
                    print("Creating directory {0}.".format(out))
                    mkdir(out)
                    print("Created directory {0}.".format(out))
                return out

            def check_resize_value(value):
                if (value < 5):
                    raise NameError("Invalid resize value: " + value)
                return str(value) + '%'

            def orientation(img):
                for k, v in img.metadata.items():
                    if k.startswith("exif:Orientation"):
                        return v

            def watermark_position(img, watermark):
                orient = orientation(img)
                print("orientation = {0}".format(orient))
                if (orient == "8"):
                    img.rotate(-90)
                    set_watermark(img, watermark)
                    img.rotate(90)
                elif (orient == "6"):
                    img.rotate(90)
                    set_watermark(img, watermark)
                    img.rotate(-90)
                else:
                    set_watermark(img, watermark)

            def set_watermark(img, watermark):
                img.watermark(watermark, transparency=0.60, left=15, top=img.height - watermark.height - 15)


            # Checks
            in_path = check_in_path(input_dir)
            out_path = create_out_path(output_dir)
            
            if watermark_file:
                watermark = Image(filename=watermark_file)

            # Run
            #print("Resizing all files from {0}. Output to {1}".format(in_path, out_path))

            #know count of files


            files = []
            for f in listdir(in_path):
                filename = join(in_path, f)
                if isfile(filename):
                    files.append(filename)

            

            len_of_files = len(files)
            self.emit(QtCore.SIGNAL("count_of_files(QString)"), "{}".format(len_of_files))
            

            progress_iter = 0
            


            for f in listdir(in_path):
                filename = join(in_path, f)
                #print(files)

                print("VIPOLNENO {} iz {}".format(progress_iter, len(files)))
                if isfile(filename):
                    #print("---\n" + filename)
                    #print("FILENAME", filename)
                    try:
                        with Image(filename=filename) as img:
                            print("Size before {0}.".format(img.size))
                            
                            if watermark_file:
                                watermark_position(img, watermark)
                            img.save(filename=output_dir + "/" + f)
                            progress_iter += 1
                            self.emit(QtCore.SIGNAL("progress_iter(QString)"), "{}".format(progress_iter))
                            #print("Size after {0}.".format(img.size)
                            
                            

                    except Exception as e:
                        print("Image processing error:", e)
                        return 1

            print("Finished processing images.")
            return 0
        add_watermark_to_image(self.watermark_file, self.input_dir, self.output_dir)



class Main(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        title = "Auto Watermarker"
        #x, y, width, height = 500, 200, 500, 500
        #self.setGeometry(x, y, width, height)
        self.setWindowTitle(title)
        self.ProgressBar = QtGui.QProgressBar(self)

        watermarmk_title = QtGui.QLabel('Пожалуйста выберите файл вотермарки')
        original_folder_title = QtGui.QLabel('Пожалуйста выберите папку с изображениями')
        output_folder_title = QtGui.QLabel('Выберите папку где будут выходные файлы')

        self.filePathWatermark = QtGui.QComboBox()
        self.filePathOriginal = QtGui.QComboBox()
        self.filePathOutput = QtGui.QComboBox()

        

        # filePathWatermark.addItem("Watermark")
        # filePathOriginal.addItem("Original")
        # filePathOutput.addItem("Output")

        self.go_btn = QtGui.QPushButton('Go', self)
        quit_btn = QtGui.QPushButton('Qiut', self)


        self.watermark_btn = FileBrowseButton("...", self)
        self.original_btn = FileBrowseButton("...", self)
        self.output_btn = FileBrowseButton("...", self)

        self.watermark_btn.clicked.connect(self.get_file)
        self.original_btn.clicked.connect(self.get_folder)
        self.output_btn.clicked.connect(self.get_output_dir)

        self.go_btn.clicked.connect(self.start)
        #self.go_btn.clicked.connect(self.runBar)
        quit_btn.clicked.connect(self.end)


        grid = QtGui.QGridLayout()

        grid.setSpacing(10)

        grid.addWidget(watermarmk_title, 1, 0)
        grid.addWidget(self.watermark_btn, 2, 0)
        grid.addWidget(self.filePathWatermark, 3, 0)

        grid.addWidget(original_folder_title, 4, 0)
        grid.addWidget(self.original_btn, 5, 0)
        grid.addWidget(self.filePathOriginal, 6, 0)

        grid.addWidget(output_folder_title, 7, 0)
        grid.addWidget(self.output_btn, 8, 0)
        grid.addWidget(self.filePathOutput, 9, 0)

        grid.addWidget(self.go_btn, 10, 0)
        grid.addWidget(quit_btn, 11, 0)
        grid.addWidget(self.ProgressBar, 12, 0)
        self.setLayout(grid)




    def get_file(self):
        file_name = QtGui.QFileDialog.getOpenFileName(self, 'Open File')
        self.filePathWatermark.addItem(file_name)
        print(file_name)

    def get_folder(self):
        directory = QtGui.QFileDialog.getExistingDirectory(self, "Select Directory")
        self.filePathOriginal.addItem(directory)
        print(directory)

    def get_output_dir(self):
        directory = QtGui.QFileDialog.getExistingDirectory(self, "Select Directory")
        self.filePathOutput.addItem(directory)
        print(directory)

    def start(self):
        self.go_btn.setDisabled(True)
        watermark = self.filePathWatermark.currentText()
        original_dir = self.filePathOriginal.currentText()
        output = self.filePathOutput.currentText()
        
        #Create Thread
        self.watermark_make_thread = MakeWatermark(watermark, original_dir, output)

        self.connect(self.watermark_make_thread, QtCore.SIGNAL("finished()"), self.on_finished)

        self.connect(
                     self.watermark_make_thread,
                     QtCore.SIGNAL("count_of_files(QString)"),
                     self.set_begin_progressbar_param,
                     QtCore.Qt.QueuedConnection
                     )

        self.connect(
                     self.watermark_make_thread,
                     QtCore.SIGNAL("progress_iter(QString)"),
                     self.update_progress_bar,
                     QtCore.Qt.QueuedConnection
                     )

        self.watermark_make_thread.start()
        

    def set_begin_progressbar_param(self, len_of_files):
        self.len_of_files = int(len_of_files)
        self.ProgressBar.setMaximum(self.len_of_files)

    def update_progress_bar(self, item):
        self.item = int(item)
        self.ProgressBar.setValue(self.item)

    def on_finished(self):
        successMsg = QtGui.QMessageBox()
        successMsg.setText("Файлы успешно созданы")
        successMsg.exec()
        self.go_btn.setDisabled(False)
        
    def end(self):
        self.close()

class FileBrowseButton(QtGui.QPushButton):
    def __init__(self, name, parent=None):
        QtGui.QPushButton.__init__(self, name, parent)
        self.setGeometry(10, 10, 20, 20)
